__author__ = 'Klein'
from flask import Flask, jsonify, request, Response
from flask_crossdomain import crossdomain
import json
from Model.Banco import db
from Service.PerguntaService import PerguntaService
from Service.UsuarioService import UsuarioService
from Service.StatusFaseService import StatusFaseService
from Service.UsuarioTokenService import UsuarioTokenService
from Service.MessageHandler import MessageHandler
from Service.HistoricoPerguntasService import HistoricoPerguntasService
from Service.JogoService import JogoService
from Service.ConquistaService import ConquistaService

application = Flask(__name__)

@application.before_request
def before_request():
   db.connect()

@application.teardown_request
def teardown_request(exception):
    if db is not None:
        db.close()

@application.route('/api/1/perguntas', methods=['GET', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def get_perguntas():

    service = PerguntaService()
    return json.dumps(service.getBlocoPerguntas())

@application.route('/api/1/pergunta/<id>', methods=['GET', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def get_pergunta(id):

    service = PerguntaService()
    return jsonify(service.getPerguntaAsJSON(id))

@application.route('/api/1/usuario', methods=['POST', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def set_usuario():

    try:
        data = request.json
        service = UsuarioService()
        token = service.criar_usuario(data)
        return str(token)
    except Exception, detalhe:
        return "Erro: " + detalhe.message

@application.route('/api/1/usuario', methods=['PUT', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def alterar_senha_usuario():

    message = MessageHandler()
    data = request.json
    service = UsuarioService()
    if service.alterarDadosUsuario(data):
        return message.getSuccessMessage("Senha alterada com sucesso")

@application.route('/api/1/usuario/<token>', methods=['GET', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def get_usuario(token):

    try:
        service = UsuarioService()
        usuario = service.get_usuario(token)
        return jsonify(usuario)
    except:
        return "Erro ao buscar o usuario"


# Pega a estrutura de fases do usuario passado no parametro
@application.route('/api/1/status_fase/usuario/<id>', methods=['GET', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def get_blocoStatusFases(id):

    usuario = UsuarioService()
    usuario.atualizarUltimoAcessoPeloID(id)
    service = StatusFaseService()
    return json.dumps(service.getBlocoFases(id))

# Atualiza as fases do usuario
@application.route('/api/1/status_fase', methods=['POST', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def set_StatusFase():

     data = request.json
     service = StatusFaseService()
     mensagem = service.atualizarStatusFase(data)
     return str(mensagem)

@application.route("/")
@crossdomain('*', headers='Content-Type')
def hello():
    message = MessageHandler()
    return message.getSuccessMessage("Endireitados!")

@application.route('/api/1/login', methods=['POST', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def login_sistema():

     data = request.json
     service = UsuarioService()
     retorno = service.logar_sistema(data)
     return str(retorno)


@application.route("/api/1/logout/<token>", methods=['DELETE', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def logout_sistema(token):

     serviceToken = UsuarioTokenService()
     retorno = serviceToken.deletar_token(token)
     return str(retorno)

@application.route('/api/1/updateHistoricoPerguntas', methods=['POST', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def updateHistoricoPerguntas():
    data = request.json
    servicePergunta = HistoricoPerguntasService()
    message = MessageHandler()
    if servicePergunta.updateHistoricoPerguntas(data):
        return message.getSuccessMessage("All updated")
    return message.getErrorMessage("Error on update historic.")

@application.route('/api/1/usuario/recuperarsenha', methods=['PUT', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def recuperar_senha_usuario():

    message = MessageHandler()
    data = request.json
    service = UsuarioService()
    if service.recuperarSenhaUsuario(data):
        return message.getSuccessMessage("Email enviado com sucesso")

@application.route('/api/1/finalizarJogo', methods=['POST', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def finalizarJogo():
    try:
        data = request.json
        serviceJogo = JogoService()
        conquista = serviceJogo.finalizarJogo(data)
        return jsonify(conquista.as_json())
    except Exception, detail:
        message = MessageHandler()
        return message.getErrorMessage("Erro ao finalizar jogo.")

@application.route('/api/1/getConquistas', methods=['POST', 'OPTIONS'])
@crossdomain('*', headers='Content-Type')
def getConquistas():
    message = MessageHandler()
    try:
        data = request.json
        service = ConquistaService()
        usuario_token = "";
        if data.has_key("token"):
            usuario_token = data['token']
        else:
            return message.getErrorMessage("Token is missing.")
        serviceUsuario = UsuarioService()
        usuario = serviceUsuario.get_usuario_by_token(usuario_token)

        conquista_array = service.listConquistas(usuario)
        return json.dumps(conquista_array)
    except Exception, detail:
        return message.getErrorMessage("Erro ao finalizar jogo.")

if __name__ == "__main__":
    application.run(host='0.0.0.0', port=5000)


