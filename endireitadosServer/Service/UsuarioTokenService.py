__author__ = 'Klein'

from Model.UsuarioToken import UsuarioToken
from uuid import uuid4
from peewee import DoesNotExist

class UsuarioTokenService:

    def __init__(self):
        print ""

    def criar_token(self, usuario):
        if usuario is None:
            return False

        usuarioToken = UsuarioToken()
        usuarioToken.usuario = usuario
        usuarioToken.token = uuid4()
        usuarioToken.save(force_insert=True)
        return usuarioToken.token

    def deletar_token(self, token):

        try:
            usuarioToken = UsuarioToken.get(UsuarioToken.token == token)
            usuarioToken.delete_instance()
            return "token excluido com sucesso"
        except DoesNotExist:
            return "token nao encontrado"

    def get_usuarioToken(self, token):

        try:
            usuarioToken = UsuarioToken.get(UsuarioToken.token == token)
            return usuarioToken
        except DoesNotExist:
            return "token nao encontrado"