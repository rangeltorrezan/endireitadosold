__author__ = 'Klein'

from flask import abort
from Model.Usuarios import Usuarios
from MessageHandler import MessageHandler
from Service.StatusFaseService import StatusFaseService
from Service.UsuarioTokenService import UsuarioTokenService
from Service.EmailService import EnvioEmail
from peewee import DoesNotExist
from datetime import datetime
import email.utils
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


class UsuarioService:

    def __init__(self):
        print ""

    def validarEmailExistente(self, email):
        try:
            Usuarios.get(Usuarios.email == email)
        except DoesNotExist:
            return True

        raise RuntimeError("o E-mail " + email + " ja existe na base de dados")


    def criar_usuario(self, data):
        usuario = Usuarios()
        if data.has_key("nome"):
            usuario.nome = data['nome']

        if data.has_key("email"):
            usuario.email = data['email']

        if data.has_key("cidade"):
            usuario.cidade = data['cidade']

        if data.has_key("estado"):
            usuario.estado = data['estado']

        if data.has_key("senha"):
            usuario.senha = data['senha']

        # caso ja exista e-mail sera disparada uma excecao runtimeError
        self.validarEmailExistente(usuario.email)

        usuario.datacriacao = datetime.now()
        usuario.dataultacesso = datetime.now()

        usuario.save(force_insert=True)
        faseService = StatusFaseService()
        faseService.criarBlocoFases(usuario.codigo)

        tokenService = UsuarioTokenService()
        token = tokenService.criar_token(usuario)

        return token

    def atualizarUltimoAcesso(self, usuario):
        if usuario is None:
            return False
        usuario.dataultacesso = datetime.now()
        usuario.save()
        return True

    def atualizarUltimoAcessoPeloID(self, id):

        try:
            usuario = Usuarios.get(Usuarios.codigo == id)
        except DoesNotExist:
            return  False

        usuario.dataultacesso = datetime.now()
        usuario.save()
        return True

    def alterarDadosUsuario(self, data):
        if data is None:
            return False

        Nome = None
        Email = None
        Cidade = None
        Estado = None
        Senha = None

        if data.has_key("nome"):
            Nome = data['nome']

        if data.has_key("email"):
            Email = data['email']

        if data.has_key("cidade"):
            Cidade = data['cidade']

        if data.has_key("estado"):
            Estado = data['estado']

        if data.has_key("senha"):
            Senha = data['senha']

        try:
            usuario = Usuarios.get(Usuarios.email == Email)
            if Senha != None:
                usuario.senha = Senha
            if Cidade != None:
                usuario.cidade = Cidade
            if Estado != None:
                usuario.estado = Estado
            if Nome != None:
                usuario.nome = Nome

            usuario.save()
            return True

        except DoesNotExist:
            raise RuntimeError("o E-mail " + Email + " nao existe na base de dados")

    def recuperarSenhaUsuario(self, data):

        if data is None:
            return False

        if data.has_key("email"):
            Email = data['email']

        try:
            usuario = Usuarios.get(Usuarios.email == Email)

            fromaddr = 'contato@endireitados.com.br'
            toaddr = Email

            msg = MIMEMultipart('alternative')

            msg.set_unixfrom('Endireitados')
            texto = 'Senha: ' + usuario.senha
            msg['To'] = email.utils.formataddr(('Recipient', toaddr))
            msg['From'] = email.utils.formataddr(('Endireitados', fromaddr))
            msg['Subject'] = 'Endireitados - Lembrete de senha'

            html = """\
                 <html>
                 <head></head>
                 <body>
                 <p>"""+texto+"""
                 </p>
                 </body>
                 </html>"""

            part1 = MIMEText(texto, 'plain')
            part2 = MIMEText(html, 'html')

            msg.attach(part1)
            msg.attach(part2)

            emailService = EnvioEmail()
            emailService.enviar_email(fromaddr, toaddr, msg)
            return True

        except DoesNotExist:
            raise RuntimeError("o E-mail " + Email + " nao existe na base de dados")


    def get_usuario(Self, token):

        tokenService = UsuarioTokenService()
        usuarioToken = tokenService.get_usuarioToken(token)
        usuario = Usuarios.get(Usuarios.codigo == usuarioToken.usuario)
        return usuario.as_json()

    def get_usuario_by_token(Self, token):
        tokenService = UsuarioTokenService()
        usuarioToken = tokenService.get_usuarioToken(token)
        usuario = Usuarios.get(Usuarios.codigo == usuarioToken.usuario)
        return usuario

    def get_usuario_by_id(self,id):
        return Usuarios.get(Usuarios.codigo == id)

    def logar_sistema(Self, data):

        if data.has_key("email"):
            email = data['email']

        if data.has_key("senha"):
            senha = data['senha']

        try:
            usuario = Usuarios.get(Usuarios.email == email, Usuarios.senha == senha)
            Self.atualizarUltimoAcesso(usuario)
            tokenService = UsuarioTokenService()
            token = tokenService.criar_token(usuario)
            return token
        except DoesNotExist:
            abort(401)