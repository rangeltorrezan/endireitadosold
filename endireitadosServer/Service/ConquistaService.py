__author__ = 'roque'

from Model.Conquista import Conquista
from Model.HistoricoPerguntas import HistoricoPerguntas
from Model.Perguntas import Perguntas
from Model.StatusFase import StatusFase
from peewee import fn

class ConquistaService:

    def __init__(self):
        print ""


    def getConquista(self, usuario, current_jogo):
        try:
            query_perguntas = Perguntas.select(Perguntas.area, (fn.sum(HistoricoPerguntas.acertou) / fn.count(HistoricoPerguntas.acertou)).alias('tx_acerto')).join(HistoricoPerguntas).where((HistoricoPerguntas.usuario == usuario) & (HistoricoPerguntas.jogo == current_jogo)).order_by( (fn.sum(HistoricoPerguntas.acertou) / fn.count(HistoricoPerguntas.acertou)) ).group_by(Perguntas.area).limit(1)
            conquista = Conquista()
            for ob in query_perguntas:
                conquista.area = ob.area
            conquista.usuario = usuario

            query_statusfase = StatusFase.select().where(StatusFase.usuario == usuario)

            total_pontos = 0
            for ob_2 in query_statusfase:
                total_pontos += ob_2.pontos

            conquista.pontos = total_pontos
            conquista.jogo = current_jogo
            conquista.save(force_insert=True)
            return conquista
        except Exception, detail:
            return False


    def listConquistas(self, usuario):
        ret = []
        query = Conquista.select().where(Conquista.usuario == usuario)
        for conquista in query:
            ret.append(conquista.as_json())
        return ret
