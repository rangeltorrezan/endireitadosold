__author__ = 'roque'


from Model.Jogo import Jogo
from Service.ConquistaService import ConquistaService
from Service.UsuarioService import UsuarioService
from Service.StatusFaseService import StatusFaseService
from datetime import datetime


class JogoService:
    insert = False

    def __init__(self):
        print ""

    def getCurrentJogo(self,usuario):
        query = Jogo.select().where(Jogo.usuario == usuario).order_by(Jogo.numero.desc()).limit(1)
        for ob in query:
            return ob

        #jogo isn't created
        return self.createJogo(usuario, 1)

    def createNewJogo(self, usuario):
        jogo_old = self.getCurrentJogo(usuario)
        jogo_old.data_termino = datetime.now()
        jogo_old.save()
        return self.createJogo(usuario, jogo_old.numero+1)

    def createJogo(self,usuario, new_numero):
        jogo = Jogo()
        jogo.numero = new_numero
        jogo.usuario = usuario
        jogo.data_inicio = datetime.now()
        jogo.save(force_insert=True)
        return jogo

    def finalizarJogo(self, data):
        usuario_token = "";
        if data.has_key("token"):
            usuario_token = data['token']
        serviceUsuario = UsuarioService()
        usuario = serviceUsuario.get_usuario_by_token(usuario_token)
        serviceConquista = ConquistaService()
        current_jogo = self.getCurrentJogo(usuario)
        conquista = serviceConquista.getConquista(usuario, current_jogo)
        self.createNewJogo(usuario)
        sfs = StatusFaseService()
        sfs.resetBlocoFases(usuario)
        return conquista