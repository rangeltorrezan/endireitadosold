__author__ = 'Klein'
from Model.Perguntas import Perguntas
from peewee import fn

class PerguntaService:

    def __init__(self):
        print ""

    # seleciona um bloco randomico de 20 perguntas
    # e retorna a lista de objetos em json
    def getBlocoPerguntas(self):
        query = Perguntas.select().order_by(fn.Rand()).limit(20)
        lista = [ob.as_json() for ob in query]
        return lista

    # seleciona uma pergunta randomica e retorna o objeto json
    def getPerguntaRandomica(self):
        query = Perguntas.select().order_by(fn.Random()).limit(1)
        obj = [ob.as_json() for ob in query]
        return obj

    # seleciona a pergunta com o id passado no parametro
    def getPerguntaAsJSON(Self, id):
        query = Perguntas.select().where(Perguntas.codigo == id)
        obj = [ob.as_json() for ob in query]
        return obj

    def getPergunta(Self, id):
        return Perguntas.get(Perguntas.codigo == id)