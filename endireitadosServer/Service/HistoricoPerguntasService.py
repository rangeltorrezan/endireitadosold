__author__ = 'roque'

from Model.HistoricoPerguntas import HistoricoPerguntas
from Service.PerguntaService import PerguntaService
from Service.UsuarioService import UsuarioService
from Service.JogoService import JogoService

import logging

class HistoricoPerguntasService:
    insert = False

    def __init__(self):
        print ""

    def updatePerguntaRespondida(self, codigoPergunta, usuario, acertou):
        self.insert = False
        try:
            serviceJogo = JogoService()
            current_jogo = serviceJogo.getCurrentJogo(usuario)
            historico = self.getHistoricoPergunta(codigoPergunta,usuario, current_jogo)

            historico.ordem = self.getNextOrdem(usuario, current_jogo)
            historico.acertou = acertou

            historico.save(force_insert=self.insert)
            return True
        except Exception, detalhe:
            logging.error("Erro ao salvar historico.")
            return False


    def getNextOrdem(self, usuario, current_jogo):
        query = HistoricoPerguntas.select().where((HistoricoPerguntas.usuario == usuario) & (HistoricoPerguntas.jogo == current_jogo)).order_by(HistoricoPerguntas.ordem.desc()).limit(1)
        for user in query:
            return user.ordem + 1
        return 0

    def getHistoricoPergunta(self,cod_pergunta, usuario, current_jogo):
        query = HistoricoPerguntas.select().where((HistoricoPerguntas.pergunta == cod_pergunta) & (HistoricoPerguntas.usuario == usuario) & (HistoricoPerguntas.jogo == current_jogo))
        for hist in query:
            return hist
        ps = PerguntaService();
        pergunta = ps.getPergunta(cod_pergunta)
        historico = HistoricoPerguntas()
        historico.pergunta = pergunta
        historico.usuario = usuario
        historico.jogo = current_jogo
        self.insert = True
        return historico

    def updateHistoricoPerguntas(self,data):
        try:
            usuario_token = "";
            if data.has_key("usuario"):
                usuario_token = data['usuario']

            serviceUsuario = UsuarioService()
            usuario = serviceUsuario.get_usuario_by_token(usuario_token)

            if data.has_key("respostas"):
                for resposta in data['respostas']:
                    if not self.updatePerguntaRespondida(resposta['pergunta'], usuario, (resposta['correto'] == "1") ):
                        return False
            return True
        except Exception, detail:
            logging.error("Error on update historics")
            return False