__author__ = 'roque'

from flask import json

class MessageHandler:
    type = ""
    message = ""

    def __init__(self):
        type = ""
        message = ""

    def getErrorMessage(self,text):
        self.type = "ERROR"
        self.message = text
        return json.dumps(self, default=lambda o: o.__dict__)

    def getWarningMessage(self,text):
        self.type = "WARNING"
        self.message = text
        return json.dumps(self, default=lambda o: o.__dict__)

    def getSuccessMessage(self,text):
        self.type = "SUCCESS"
        self.message = text
        return json.dumps(self, default=lambda o: o.__dict__)