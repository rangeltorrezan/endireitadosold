__author__ = 'Klein'
from Model.StatusFase import StatusFase
from MessageHandler import MessageHandler

class StatusFaseService:

    def __init__(self):
        print ""

    # seleciona o bloco das fases do usuario e retorna a lista de objetos em json
    def getBlocoFases(self, id):
        query = StatusFase.select().where(StatusFase.usuario == id).order_by(StatusFase.codigo)
        lista = [ob.as_json() for ob in query]
        return lista

    # Atualiza a fase de acordo com o json passado no parametro
    def atualizarStatusFase(self, data):
        message = MessageHandler()
        try:
            for status in data:
                fase = StatusFase.get(StatusFase.usuario == status['usuario'], StatusFase.codigo == status['codigo'])
                fase.status = status['status']
                fase.aproveitamento = status['aproveitamento']
                fase.chances = status['chances']
                fase.pontos = status['pontos']
                fase.save()

            return message.getSuccessMessage("Fases atualizadas com sucesso")
        except:
            return message.getErrorMessage("Houve erro ao atualizar as fases")

    # Exclui os registros de fase do usuario passado no parametro
    def excluirBlocoFases(self, id):
        StatusFase.delete().where(StatusFase.codigo == id)

    # Faz um loop das fases definidas e seta as propriedades padrao
    def criarBlocoFases(self, usuario):
        codigoFase = 1
        while codigoFase <= 7:
            status = StatusFase()
            status.codigo = codigoFase

            if codigoFase == 1:
                status.status = 'play'
            else:
                status.status = 'block'

            status.aproveitamento = 0
            status.chances = 0
            status.pontos = 0
            status.usuario = usuario

            status.save(force_insert=True)
            codigoFase += 1

    def resetBlocoFases(self,usuario):
        codigoFase = 1
        query = StatusFase.select().where(StatusFase.usuario == usuario).order_by(StatusFase.codigo)
        for status in query:
            status.codigo = codigoFase

            if codigoFase == 1:
                status.status = 'play'
            else:
                status.status = 'block'

            status.aproveitamento = 0
            status.chances = 0
            status.pontos = 0

            status.save()
            codigoFase += 1



