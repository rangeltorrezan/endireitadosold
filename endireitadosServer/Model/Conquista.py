__author__ = 'roque'

from BaseModel import MySQLModel
from Model.Usuarios import Usuarios
from Model.Jogo import Jogo
from peewee import ForeignKeyField, TextField, IntegerField, CompositeKey
from Banco import db

class Conquista(MySQLModel):
    usuario = ForeignKeyField(Usuarios)
    jogo = ForeignKeyField(Jogo)
    area = TextField()
    pontos = IntegerField()

    class Meta:
        primary_key = CompositeKey('usuario', 'jogo')
        database = db

    def as_json(self):
        return dict(
            jogo=self.jogo.numero,
            area=self.area,
            pontos=self.pontos)


db.create_tables([Conquista],True)