__author__ = 'Klein'

from BaseModel import MySQLModel
from Model.Usuarios import Usuarios
from peewee import PrimaryKeyField, ForeignKeyField, CharField
from Banco import db

class UsuarioToken(MySQLModel):
    codigo = PrimaryKeyField()
    usuario = ForeignKeyField(Usuarios, related_name='usuarios')
    token = CharField(max_length=100)

    class Meta:
        database = db

    def as_json(self):
        return dict(
            codigo=self.codigo, usuario=self.usuario, token=self.token)

db.create_tables([UsuarioToken], True)