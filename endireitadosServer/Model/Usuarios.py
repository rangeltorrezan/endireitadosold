__author__ = 'Klein'
from BaseModel import MySQLModel
from peewee import PrimaryKeyField, TextField, CharField, DateTimeField
from Banco import db

class Usuarios(MySQLModel):
    codigo = PrimaryKeyField()
    nome = CharField()
    email = CharField()
    cidade = CharField()
    estado = CharField(max_length=2)
    senha = CharField(max_length=100)
    datacriacao = DateTimeField()
    dataultacesso = DateTimeField()

    class Meta:
        database = db

    def as_json(self):
        return dict(
            codigo=self.codigo, nome=self.nome,
            email=self.email, cidade=self.cidade,
            estado=self.estado, senha=self.senha,
            datacriacao=self.datacriacao,
            dataultacesso=self.dataultacesso)

