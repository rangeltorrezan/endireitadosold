__author__ = 'Klein'
from BaseModel import MySQLModel
from Model.Usuarios import Usuarios
from peewee import IntegerField, CharField, ForeignKeyField
from Banco import db


class StatusFase(MySQLModel):
    codigo = IntegerField()
    status = CharField(max_length=10)
    chances = IntegerField()
    pontos = IntegerField()
    aproveitamento = IntegerField()
    usuario = ForeignKeyField(Usuarios, related_name='fases')

    class Meta:
        database = db

    def as_json(self):
        return dict(
            codigo=self.codigo, status=self.status,
            chances=self.chances, pontos=self.pontos,
            aproveitamento=self.aproveitamento, usuario=self.usuario.codigo)
