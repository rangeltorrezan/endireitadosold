__author__ = 'Klein'

from BaseModel import MySQLModel
from peewee import PrimaryKeyField, TextField, CharField, IntegerField
from Banco import db

class Perguntas(MySQLModel):
    codigo = PrimaryKeyField()
    pergunta = TextField()
    resposta1 = TextField()
    resposta2 = TextField()
    resposta3 = TextField()
    resposta4 = TextField()
    respCorreta = CharField()
    explicacao = TextField()
    area = TextField()
    url = TextField()
    prova = TextField()
    numeroQuestao = IntegerField()

    class Meta:
        database = db

    def as_json(self):
        return dict(
            codigo=self.codigo, pergunta=self.pergunta,
            resposta1=self.resposta1, resposta2=self.resposta2,
            resposta3=self.resposta3, resposta4=self.resposta4,
            respCorreta=self.respCorreta, explicacao = self.explicacao,
            area = self.area, url = self.url,
            prova = self.prova, numeroQuestao = self.numeroQuestao)
