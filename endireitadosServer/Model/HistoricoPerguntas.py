__author__ = 'roque'

from BaseModel import MySQLModel
from peewee import IntegerField, BooleanField, ForeignKeyField, CompositeKey
from Model.Usuarios import Usuarios
from Model.Perguntas import Perguntas
from Model.Jogo import Jogo
from Banco import db

class HistoricoPerguntas(MySQLModel):
    usuario = ForeignKeyField(Usuarios)
    pergunta = ForeignKeyField(Perguntas)
    jogo = ForeignKeyField(Jogo)
    ordem = IntegerField()
    acertou = BooleanField()

    class Meta:
        primary_key = CompositeKey('pergunta', 'usuario', 'jogo')
        database = db

    def as_json(self):
        return dict(
            usuario=self.usuario,
            pergunta=self.pergunta,
            jogo=self.jogo,
            ordem=self.ordem,
            acertou=self.acertou)

db.create_tables([HistoricoPerguntas],True)