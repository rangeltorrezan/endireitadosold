__author__ = 'Klein'
from BaseModel import MySQLModel
from Model.Usuarios import Usuarios
from peewee import DateTimeField, ForeignKeyField, CompositeKey, IntegerField, PrimaryKeyField
from Banco import db

class Jogo(MySQLModel):
    id = PrimaryKeyField()
    numero = IntegerField()  # numero do jogo do usuario
    usuario = ForeignKeyField(Usuarios)
    data_inicio = DateTimeField()
    data_termino = DateTimeField()

    class Meta:
        database = db

    def as_json(self):
        return dict(
            id=self.id,
            numero=self.numero,
            data_inicio=self.data_inicio,
            data_termino=self.data_termino)

db.create_tables([Jogo],True)