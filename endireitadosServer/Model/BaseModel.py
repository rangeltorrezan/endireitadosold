__author__ = 'Klein'

from Banco import db
from peewee import Model, DoesNotExist

class MySQLModel(Model):
    """A base model that will use our MySQL database"""
    class Meta:
        database = db

    def get_object_or_404(model, *expressions):
        try:
            return model.get(*expressions)
        except model.DoesNotExist:
            return "registro nao encontrado"