angular.module('endireitados',[])

.controller('ApiCtrl', function ($scope, $http) {
	//var urlEndpoint = "http://localhost:5000/api/1";
	var urlEndpoint = "http://endireitados.elasticbeanstalk.com/api/1";

	$scope.getPerguntas = function(){
		$http.get(urlEndpoint + "/perguntas").
		  success(function(data) {
		    $scope.perguntas = data;
		  }).
		  error(function(data, status, headers, config) {
		    $scope.msg = "Data: " + data + " Status: " + status + " Headers: " + headers;
	  	});		
	}


	$scope.getUsuario = function(token){
		$http.get(urlEndpoint + "/usuario/" + token).
		  success(function(data) {
		    $scope.usuario = data;
		  }).
		  error(function(data, status, headers, config) {
		    $scope.msg = "Data: " + data + " Status: " + status + " Headers: " + headers;
	  	});		
	}

	$scope.cadastroUsuario = function(novoUsuario){
		$http.post(urlEndpoint + "/usuario", novoUsuario).
		  success(function(data) {
		    $scope.tokenNovo = data;
		  }).
		  error(function(data, status, headers, config) {
		    $scope.msg = {"Data":data,"Status":status,"Headers":headers};
	  	});		
	}
	
	$scope.salvaFase = function(fase){
		$http.post(urlEndpoint + "/status_fase", fase).
		  success(function(data) {
		    $scope.fase = data;
		  }).
		  error(function(data, status, headers, config) {
		    $scope.msg = {"Data":data,"Status":status,"Headers":headers};
	  	});		
	}

	$scope.loginUsuario = function(login){
		$http.post(urlEndpoint + "/login", login).
		  success(function(data) {
		    $scope.tokenvalido = data;
		  }).
		  error(function(data, status, headers, config) {
		    $scope.msg = {"Data":data,"Status":status,"Headers":headers};
	  	});		
	}

	
	$scope.dadosFase = function(codUsuario){
		$http.get(urlEndpoint + "/status_fase/usuario/" + codUsuario).
		  success(function(data) {
		    $scope.dadosFase = data;
		  }).
		  error(function(data, status, headers, config) {
		    $scope.msg = {"Data":data,"Status":status,"Headers":headers};
	  	});		
	}

	$scope.logoutUsuario = function(token){
		$http.delete(urlEndpoint + "/logout/" + token).
		  success(function(data) {
			$scope.msgRetorno = data;
		  }).
		  error(function(data, status, headers, config) {
			$scope.msg = {"Data":data,"Status":status,"Headers":headers};
		});		
	}
	
	$scope.updateHistoricoPerguntas = function(){
		var json_perguntas = {
					'usuario' : 'dfaaa7fe-63ba-4134-9f84-fe918b5a3676',
					'respostas' :[{ 'pergunta' : '1' , 'correto' : '0' },{ 'pergunta' : '2' , 'correto' : '1' }]
		};
		
		$http.post(urlEndpoint + "/updateHistoricoPerguntas", json_perguntas).
		success(function(data) {
			$scope.msgRetorno = data;
		}).
		error(function(data, status, headers, config) {
			$scope.msg = {"Data":data,"Status":status,"Headers":headers};
		});		
	}

        
	$scope.recuperarSenha = function(email){
		var recuperarEmail = {"email":email};
		console.log(email);
		$http.put(urlEndpoint + "/usuario/recuperarsenha", recuperarEmail).
		  success(function(data) {
			$scope.recupSenha = data;
		  }).error(function(data, status, headers, config) {
			$scope.msg = {"Data":data,"Status":status,"Headers":headers};
		});
	}

	$scope.finalizarJogo = function(token){
		var json_finalizarJogo = { 'token' : token };
		$http.post(urlEndpoint + "/finalizarJogo", json_finalizarJogo).
		  success(function(data) {
			$scope.retFinalizarJogo = data;
		  }).error(function(data, status, headers, config) {
			$scope.msg = {"Data":data,"Status":status,"Headers":headers};
		});		
	}

	$scope.getConquistas = function(token){
		var json_conquistas = { 'token' : token };
		$http.post(urlEndpoint + "/getConquistas", json_conquistas).
		  success(function(data) {
			$scope.retConquistas = data;
		  }).error(function(data, status, headers, config) {
			$scope.msg = {"Data":data,"Status":status,"Headers":headers};
		});		
	}

});

//Informações

/*

Setting HTTP Headers
The $http service will automatically add certain HTTP headers to all requests. 
These defaults can be fully configured by accessing the $httpProvider.defaults.headers 
configuration object, which currently contains this default configuration:

$httpProvider.defaults.headers.common (headers that are common for all requests):
	Accept: application/json, text/plain, * / *

$httpProvider.defaults.headers.post: (header defaults for POST requests)
	Content-Type: application/json

$httpProvider.defaults.headers.put (header defaults for PUT requests)
	Content-Type: application/json

*/
