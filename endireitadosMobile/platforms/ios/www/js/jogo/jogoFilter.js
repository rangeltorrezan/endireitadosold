(function() {
    'use strict';

	angular.module('jogo')
		//Para montar as vidas sem ter que repetir varias vezes - Contador
		.filter('range', function() {
		  return function(input, total) {
		    total = parseInt(total);
		    for (var i=0; i<total; i++)
		      input.push(i);
		    return input;
		  };
		})

		// Necessário par pertir abrir um link - Video
		.filter('trusted', ['$sce', function ($sce) {
		    return function(url) {
		        return $sce.trustAsResourceUrl(url);
		    };
		}])
})();