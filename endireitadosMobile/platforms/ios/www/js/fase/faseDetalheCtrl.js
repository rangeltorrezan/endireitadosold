(function() {
    'use strict';

	angular.module('fase')
	  
	    .controller('FaseDetalheCtrl', function($scope, $ionicLoading, FaseAPI) {
		
			if(typeof analytics !== "undefined") { analytics.trackView("Introdução"); }
			
			$scope.dadosFase;
			var dadosUsuario;

			dadosUsuario = angular.fromJson(window.localStorage['dadosUsuario']);
			$scope.dadosFases = angular.fromJson(window.localStorage['fases']);
			if (angular.fromJson(window.localStorage['atualizaFase']) == true){
				FaseAPI.salvaFase($scope.dadosFases).success(function(){
					window.localStorage['atualizaFase'] = false;
					$ionicLoading.hide();
				}).error(function(){
					$ionicLoading.hide();
				});
			}
	});
})();
