(function() {
    'use strict';
	angular.module('dataservice')

		.service('UsuarioAPI', function($http){
			this.novoUsuario = function(novoUsuario){
				return  $http.post(urlEndpoint + "/usuario", novoUsuario);
			},

			this.dadosUsuario = function(token){
				return $http.get(urlEndpoint + "/usuario/" + token);
			},

			this.loginUsuario = function(login){
				return $http.post(urlEndpoint + "/login", login);
			},

			this.logoutUsuario = function(token){
				return $http.delete(urlEndpoint + "/logout/" + token);
			},

			this.recuperaSenha = function(email){
				return $http.put(urlEndpoint + "/usuario/recuperarsenha", email);
			}
		})
})();