(function() {
  'use strict';

  angular.module('usuario')

    .controller('UsuarioCtrl', function($scope, $ionicLoading, $window, $state, UsuarioAPI) {
      if (typeof $window.analytics !== 'undefined') {
        $window.analytics.trackView('Conta Usuario');
        }
      $scope.dadosUsuario = angular.fromJson(window.localStorage['dadosUsuario']);


      $scope.doNewAccount = function(novoUsuario) {
        if (typeof $window.analytics !== 'undefined') {
                $window.analytics.trackView('Nova Conta');
        }

        $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        
        if(novoUsuario.senha == novoUsuario.senha2){
          UsuarioAPI.novoUsuario(novoUsuario).success(function(data) {
            $scope.armazenaDadosUsuario(data);
            $ionicLoading.hide();
          }).error(function(data, status, headers, config) {
            $scope.msg = {"Data":data,"Status":status,"Headers":headers};
            $ionicLoading.hide();
          });
        }
        else{
          $state.go("cadastro");
          $scope.msgErro = "Senhas não conferem";
          $ionicLoading.hide();
        }
      };

      $scope.recuperaSenha = function(email){
        $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        var recuperaEmail = {"email":email};
        UsuarioAPI.recuperaSenha(recuperaEmail).success(function(data) {
          $scope.msg = data;
          $ionicLoading.hide();
          $state.go("recuperaSenha");
        }).error(function(data){
          console.log("teste");
          $scope.msgErro = data;
          $ionicLoading.hide();
          $state.go("recuperaSenha");
        })

      }

      $scope.armazenaDadosUsuario = function(token){
        UsuarioAPI.dadosUsuario(token).success(function(data) {
          window.localStorage['dadosUsuario'] = JSON.stringify(data);
          window.localStorage['token'] = JSON.stringify(token);
          $state.go("app.fases");
        }).error(function (data, status, headers, config) {
          $state.go("login");
        }); 
      }
    });
})();
