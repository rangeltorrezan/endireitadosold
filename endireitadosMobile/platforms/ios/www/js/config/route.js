(function() {
    'use strict';

  angular.module('config')

  .config(function($stateProvider, $urlRouterProvider) {
    	$stateProvider

      .state('intro', {
        url: "/intro",
        templateUrl: "templates/intro.html",
        controller: 'IntroCtrl'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      })
      .state('logout', {
        url: '/logout',
        templateUrl: 'templates/login.html',
        controller: 'LogoutCtrl'
      })
      .state('cadastro', {
        url: "/cadastro",
        templateUrl: "templates/cadastro.html",
        controller: 'UsuarioCtrl'
      })

      .state('recuperaSenha', {
        url: "/recuperasenha",
        templateUrl: "templates/recuperasenha.html",
        controller: 'UsuarioCtrl'
      })


      .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
      })

      .state('app.conta', {
        url: "/conta",
        views: {
          'menuContent' :{
            templateUrl: "templates/conta.html",
            controller: 'UsuarioCtrl'
          }
        }
      })

      .state('app.ranking', {
        url: "/ranking",
        views: {
          'menuContent' :{
            templateUrl: "templates/ranking.html"
          }
        }
      })
      .state('app.fases', {
        url: "/fases",
        views: {
          'menuContent' :{
            templateUrl: "templates/fases.html",
            controller: 'FasesCtrl'
          }
        }
      })

      .state('app.perdeufase', {
        url: "/perdeufase",
        views: {
          'menuContent' :{
            templateUrl: "templates/perdeu_fase.html",
            controller: 'FaseDetalheCtrl'
          }
        }
      })


      .state('app.ganhoufase', {
        url: "/ganhoufase",
        views: {
          'menuContent' :{
            templateUrl: "templates/ganhou_fase.html",
            controller: 'FaseDetalheCtrl'
          }
        }
      })

      .state('app.conquistas', {
        url: "/conquistas",
        views: {
          'menuContent' :{
            templateUrl: "templates/conquista.html",
            controller: 'ConquistaCtrl'
          }
        }
      })

      .state('app.ganhoujogo', {
        url: "/ganhoujogo",
        views: {
          'menuContent' :{
            templateUrl: "templates/ganhou_jogo.html",
            controller: 'FimJogoCtrl'
          }
        }
      })

      .state('app.jogo', {
        url: "/jogo/fase/:fase",
        views: {
          'menuContent' :{
            templateUrl: "templates/jogo.html",
            controller: 'JogoCtrl'
          }
        }
      });
    
    // if none of the above states are matched, use this as the fallback
    var dadosUsuario = angular.fromJson(window.localStorage['dadosUsuario'])
    var intro = angular.fromJson(window.localStorage['intro'])
    if(dadosUsuario){
      $urlRouterProvider.otherwise('/app/fases');
    }
    else {
      $urlRouterProvider.otherwise('/intro');
    }
  });

})();
