(function() {
    'use strict';

	angular.module('dataservice')
		.service('FaseAPI', function ($http) {
			this.dadosFase = function(codUsuario){
				return $http.get(urlEndpoint + "/status_fase/usuario/" + codUsuario);
			},

			this.salvaFase = function (dadosFase){
				return $http.post(urlEndpoint + "/status_fase", dadosFase);
			},

			this.salvaHistoricoFase = function(){
				var perguntas =  {
					usuario : JSON.parse(window.localStorage['token']),
					respostas : JSON.parse(window.localStorage['respostas'])
				};
				return $http.post(urlEndpoint + "/updateHistoricoPerguntas", perguntas);
			}

		});
})();