(function() {
  'use strict';

  angular.module('usuario')

    .controller('LogoutCtrl', function($scope, $ionicModal, $timeout, $state, $ionicLoading, $window, UsuarioAPI) {

        $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });

        UsuarioAPI.logoutUsuario(angular.fromJson(window.localStorage['token'])).success(function(){
          delete window.localStorage['dadosUsuario'];
          delete window.localStorage['atualizaFase'];
          delete window.localStorage['jogoAtual'];
          delete window.localStorage['respostas'];
          delete window.localStorage['token'];
          $ionicLoading.hide();
          $state.go("login");

        }).error(function(){
          
        })


      $scope.armazenaDadosUsuario = function(token){
        UsuarioAPI.dadosUsuario(token).success(function(data) {
          window.localStorage['dadosUsuario'] = JSON.stringify(data);
          window.localStorage['token'] = JSON.stringify(token);
          $ionicLoading.hide();
          $state.go("app.fases");
        }).error(function (data, status, headers, config) {
          $state.go("login");
        }); 
      }

    })
})();