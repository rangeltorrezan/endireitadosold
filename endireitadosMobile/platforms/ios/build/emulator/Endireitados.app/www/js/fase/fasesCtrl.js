(function() {
    'use strict';

	angular.module('fase')

		.controller('FasesCtrl', function($scope, $ionicLoading, $window, FaseAPI, JogoAPI) {
			if (typeof $window.analytics !== 'undefined') {
		            $window.analytics.trackView('Fase');
		    }

			$ionicLoading.show({
			    content: 'Loading',
			    animation: 'fade-in',
			    showBackdrop: true,
			    maxWidth: 200,
			    showDelay: 0
		  	
		  	});
			var dadosUsuario;

			dadosUsuario = angular.fromJson(window.localStorage['dadosUsuario']);
			$scope.dadosFases = angular.fromJson(window.localStorage['fases']);
			$scope.jogoAtual = angular.fromJson(window.localStorage['jogoAtual']);
			
			if(dadosUsuario && angular.fromJson(window.localStorage['atualizaFase']) == true){
				FaseAPI.salvaFase($scope.dadosFases).success(function(){
					window.localStorage['atualizaFase'] = false;
					$ionicLoading.hide();
				}).error(function(){
					$ionicLoading.hide();
				});
			}
			else if (dadosUsuario && angular.fromJson(window.localStorage['atualizaFase']) == false) {	
				FaseAPI.dadosFase(dadosUsuario.codigo).success(function(data){
				    window.localStorage['fases'] = JSON.stringify(data);
				    $scope.dadosFases = data;
			  		$ionicLoading.hide();
			  	})
			  	.error(function(data, status, headers,config){
				    console.log('data error');
				    $ionicLoading.hide();
			  	});	
			}
			else if (angular.fromJson(window.localStorage['atualizaFase']) == true){
				$ionicLoading.hide();

			}
			else if (angular.fromJson(window.localStorage['atualizaFase']) == false){
				$ionicLoading.hide();
			}
		});
})();	