(function() {
  'use strict';
  
  angular.module('usuario')

    .controller('LoginCtrl', function($scope, $ionicModal, $timeout, $state, $ionicLoading, $window, UsuarioAPI) {
      $scope.doLogin = function(login) {
        if (typeof $window.analytics !== 'undefined') {
          $window.analytics.trackView('Login');
        }

        $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });

        UsuarioAPI.loginUsuario(login).success(function(data, status){
          $scope.armazenaDadosUsuario(data);
        }).error(function(){
          $ionicLoading.hide();
          $state.go("login");
          $scope.msgErro = "Não foi possivel efetuar o login";
        });

      };

      $scope.armazenaDadosUsuario = function(token){
        UsuarioAPI.dadosUsuario(token).success(function(data) {
          window.localStorage['dadosUsuario'] = JSON.stringify(data);
          window.localStorage['token'] = JSON.stringify(token);
          window.localStorage['atualizaFase'] = true;
          $ionicLoading.hide();
          $state.go("app.fases");
        }).error(function (data, status, headers, config) {
          $state.go("login");
        }); 
      }

    })
})();