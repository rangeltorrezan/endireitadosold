(function() {
  'use strict';

  angular.module('app', [ 
          //Modulo Padrão do Ionic
          'ionic',

          //Modulo de Features 
          'fase',
          'jogo',
          'config',
          'usuario',
          'conquista',
          'dataservice'
  ])

  .run(function($ionicPlatform, $window) {
    $ionicPlatform.ready(function() {
      if (typeof $window.analytics !== 'undefined') {
        $window.analytics.startTrackerWithId('UA-58430076-1');
      }

      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if(window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  });
})();