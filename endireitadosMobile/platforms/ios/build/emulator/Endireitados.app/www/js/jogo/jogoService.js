(function() {
    'use strict';

	angular.module('jogo')
    
	.factory('Jogo', function () {
		return {
			configuracao: function(fase) {
				var jogoAtual = angular.fromJson(window.localStorage['jogoAtual']);
				var config;
				if(jogoAtual){
					config = jogoAtual;
				}
				else{
					config = {vidas:8 - fase, respostasErradas:0, perguntaAtual: 0, fase: fase}
					window.localStorage['jogoAtual'] = JSON.stringify(config);
				}
				return config
			},

			respostaCorreta: function(fase, dadosFases, jogoAtual) {
				dadosFases[fase-1].pontos = parseInt(dadosFases[fase-1].pontos) + 10;
				window.localStorage['fases'] = JSON.stringify(dadosFases);
				jogoAtual.perguntaAtual ++;
				window.localStorage['jogoAtual'] = JSON.stringify(jogoAtual);
				return dadosFases
			},

			respostaErrada: function(fase, dadosFases, jogoAtual){
				dadosFases[fase-1].pontos = parseInt(dadosFases[fase-1].pontos) - 5;
				window.localStorage['fases'] = JSON.stringify(dadosFases);
				jogoAtual.perguntaAtual ++;
				jogoAtual.vidas --;
				jogoAtual.respostasErradas ++;
				window.localStorage['jogoAtual'] = JSON.stringify(jogoAtual);
				return dadosFases
			},

			venceuFase: function (dadosJogo, dadosFases) {
				dadosFases[dadosJogo.fase-1].status = "played";
				if(dadosJogo.fase != 7){
					dadosFases[dadosJogo.fase].status = "play";
				}
				dadosFases[dadosJogo.fase-1].aproveitamento  = ((20 - dadosJogo.respostasErradas)*100)/20;
				dadosUsuario = angular.fromJson(window.localStorage['dadosUsuario']);
				
				if(dadosUsuario){
					dadosUsuario.pontuacao = parseInt(dadosUsuario.pontuacao) + parseInt(dadosFases[dadosJogo.fase-1].pontos);
					window.localStorage['dadosUsuario'] = JSON.stringify(dadosUsuario);
				}
				window.localStorage['fases'] = JSON.stringify(dadosFases);
				return dadosFases
			},

			perdeuFase: function (dadosJogo, dadosFases) {

				if (dadosJogo.fase == 1 && parseInt(dadosFases[dadosJogo.fase - 1].chances) < 3) {
					dadosFases[dadosJogo.fase - 1].chances = parseInt(dadosFases[dadosJogo.fase - 1].chances) + 1;
					dadosFases[dadosJogo.fase - 1].pontos = 0;
					dadosFases[dadosJogo.fase - 1].aproveitamento = 0;
				}

				else if (dadosJogo.fase == 1 && parseInt(dadosFases[dadosJogo.fase - 1].chances) == 3) {
					dadosFases[dadosJogo.fase - 1].chances = 0;
					dadosFases[dadosJogo.fase - 1].pontos = 0;
					dadosFases[dadosJogo.fase - 1].aproveitamento = 0;
				}

				else if (dadosJogo.fase != 1 && parseInt(dadosFases[dadosJogo.fase - 1].chances) >= 3 ) {
					dadosFases[dadosJogo.fase - 1].status = "block";
					dadosFases[dadosJogo.fase - 1].chances = 0;
					dadosFases[dadosJogo.fase - 1].pontos = 0;
					dadosFases[dadosJogo.fase - 1].aproveitamento = 0;
					dadosFases[dadosJogo.fase - 2].status = "play";
					dadosFases[dadosJogo.fase - 2].pontos = 0;
					dadosFases[dadosJogo.fase - 2].chances = 0;
					dadosFases[dadosJogo.fase - 2].aproveitamento = 0;

				}

				else if (dadosJogo.fase != 1 && parseInt(dadosFases[dadosJogo.fase - 1].chances) < 3) {
					dadosFases[dadosJogo.fase - 1].pontos = 0;
					dadosFases[dadosJogo.fase - 1].chances = parseInt(dadosFases[dadosJogo.fase - 1].chances) + 1;
					dadosFases[dadosJogo.fase - 1].aproveitamento = 0;
				}
				window.localStorage['fases'] = JSON.stringify(dadosFases);
				return dadosFases
			}
		};	
	})
})();
