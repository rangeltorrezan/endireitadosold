(function() {
    'use strict';

	angular.module('dataservice')
		.service('JogoAPI', function($http){
			this.finalizaJogo = function(token){
				var json_finalizarJogo = { 'token' : token };
				return $http.post(urlEndpoint + "/finalizarJogo", json_finalizarJogo);
			}

			this.getPerguntas = function(){
				return $http.get(urlEndpoint + "/perguntas");	
			}
		});
})();