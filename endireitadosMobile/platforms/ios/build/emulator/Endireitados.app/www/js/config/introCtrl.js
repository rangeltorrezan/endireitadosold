(function() {
    'use strict';

	angular.module('config')

	.controller('IntroCtrl', function($scope, Fase){
		if(typeof analytics !== "undefined") { analytics.trackView("Introdução"); }
		window.localStorage['intro'] =  true;
		window.localStorage['atualizaFase'] =  false;
		$scope.dadosFases = Fase.inicializa();
		window.localStorage['fases'] = JSON.stringify($scope.dadosFases);
		window.localStorage['respostas'] = JSON.stringify([]);
	});
})();