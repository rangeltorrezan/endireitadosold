(function() {
    'use strict';

	angular.module('fase')
		.factory('Fase', function () {
			return {
				inicializa: function(fase) {
					var config = 
						[
							{ 
								"status": "play", 
								"aproveitamento": 0, 
								"usuario": 128, 
								"chances": 0, 
								"codigo": 1, "pontos": 0
							}, 
							{
								"status": "block", 
								"aproveitamento": 0, 
								"usuario": 128, 
								"chances": 0, 
								"codigo": 2, "pontos": 0
							}, 
							{
								"status": "block", 
								"aproveitamento": 0, 
								"usuario": 128, 
								"chances": 0, 
								"codigo": 3, 
								"pontos": 0
							}, 
							{
								"status": "block", 
								"aproveitamento": 0, 
								"usuario": 128, 
								"chances": 0, 
								"codigo": 4, 
								"pontos": 0
							}, 
							{
								"status": "block", 
								"aproveitamento": 0, 
								"usuario": 128, 
								"chances": 0, 
								"codigo": 5, 
								"pontos": 0
							}, 
							{
								"status": "block", 
								"aproveitamento": 0, 
								"usuario": 128, 
								"chances": 0, 
								"codigo": 6, "pontos": 0
							}, 
							{
								"status": "block", 
								"aproveitamento": 0, 
								"usuario": 128, 
								"chances": 0, 
								"codigo": 7, 
								"pontos": 0
							}
						]
					return config
				}
			}
		})

})();