(function() {
    'use strict';


    angular.module('jogo')

      .controller('JogoCtrl', function($scope, $http, $ionicPlatform, $ionicScrollDelegate, $window, $rootScope, $state, $ionicPopup, $stateParams, $ionicModal, $timeout, $ionicLoading, Jogo, JogoAPI, FaseAPI) {
        //API Google Analytics
        if (typeof $window.analytics !== 'undefined') { $window.analytics.trackView('Jogo');}
        
        
        $scope.emailUsuario = angular.fromJson(window.localStorage['dadosUsuario']);
        $scope.perguntas = [];
        $scope.dadosJogo = Jogo.configuracao($state.params.fase);
        $scope.correta;
        $scope.errada;
        $scope.videoLink;
        $scope.cronometro = 180;
        $scope.msgTempo;
        $scope.respostas = [];
        var cron;
        
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });



        $scope.avancar = function(){
          if($scope.dadosJogo.perguntaAtual == 19 && $scope.dadosJogo.fase != 7){
            Jogo.venceuFase($scope.dadosJogo, angular.fromJson(window.localStorage['fases']));
            window.localStorage['atualizaFase'] =  true;
            delete window.localStorage['jogoAtual'];
            $state.go("app.ganhoufase");
          }

          else if($scope.dadosJogo.perguntaAtual == 19 && $scope.dadosJogo.fase == 7){
            Jogo.venceuFase($scope.dadosJogo, angular.fromJson(window.localStorage['fases']));
            window.localStorage['atualizaFase'] =  true;
            delete window.localStorage['jogoAtual'];
            $state.go("app.ganhoujogo");
          }
          else if($scope.dadosJogo.vidas <= 0) {
              Jogo.perdeuFase($scope.dadosJogo, angular.fromJson(window.localStorage['fases']));
              window.localStorage['atualizaFase'] =  true;
              delete window.localStorage['jogoAtual'];
              $state.go("app.perdeufase");
          }
          else{
            $scope.dadosJogo.perguntaAtual ++;
            delete $scope.correta;
            delete $scope.errada;
            delete $scope.msg;
            delete $scope.msgTempo;
            $scope.scrollTop();
            $scope.cronometro = 180;
            cronometro();
          }
        };

        $scope.itemClicked = function ($index) {
          $scope.selectedIndex = $index;
        };


        var cronometro = function(){ 
          cron = $timeout (function(){
            if($scope.cronometro == 0){
              $scope.selectedIndex = 'zero';
              $scope.msgTempo = true;
              $scope.verificaResposta();
            }
            else{ 
              $scope.cronometro --;
              cronometro();
            }
          }, 1000);
        }
        

        var cronometroStop = function(){
          $timeout.cancel(cron);
        }
        

        if (window.localStorage['respostas'] && window.localStorage['token'] ){
          FaseAPI.salvaHistoricoFase().success(function(data){
            window.localStorage['respostas'] = JSON.stringify([]);
          })
        }
        else if(window.localStorage['token']){
            window.localStorage['respostas'] = JSON.stringify([]);
        }

        JogoAPI.getPerguntas().success(function(data){
          $scope.perguntas =  data;
          $ionicLoading.hide();
          cronometro();
        })
        

        $scope.scrollTop = function() {
          $ionicScrollDelegate.scrollTop();
        };

        $scope.openVideo = function(video) {
          if (typeof $window.analytics !== 'undefined') { $window.analytics.trackView('video');}
          $ionicModal.fromTemplateUrl('video.html', {
            scope: $scope,
            backdropClickToClose: false,
            hardwareBackButtonClose: false
            }).then(function(modal) {
              $scope.modal = modal;
              $scope.videoLink = video;
              $scope.modal.show();
          });
        };

        $scope.historicoPerguntas = function (resp){
          var respostasHist = [];
          respostasHist = JSON.parse(window.localStorage['respostas']);
          respostasHist.push({pergunta : $scope.perguntas[$scope.dadosJogo.perguntaAtual].codigo , correto : resp});
          window.localStorage['respostas'] = JSON.stringify(respostasHist);
        };

        $scope.verificaResposta = function (){
          $ionicScrollDelegate.scrollBottom();
          cronometroStop();
          if($scope.perguntas[$scope.dadosJogo.perguntaAtual].respCorreta == ($scope.selectedIndex + 1)){
            Jogo.respostaCorreta($state.params.fase, angular.fromJson(window.localStorage['fases']), angular.fromJson(window.localStorage['jogoAtual']));
            $scope.historicoPerguntas('1');
            $scope.correta = $scope.selectedIndex;
            delete $scope.selectedIndex;
            $scope.msg = "correta";
          }
          else {
            $scope.dadosJogo.vidas --;
            $scope.dadosJogo.respostasErradas ++;
            Jogo.respostaErrada($state.params.fase, angular.fromJson(window.localStorage['fases']), angular.fromJson(window.localStorage['jogoAtual']));
            $scope.historicoPerguntas('0');   
            $scope.msg = "errada";
            $scope.errada = $scope.selectedIndex;
            delete $scope.selectedIndex;
            $scope.correta = $scope.perguntas[$scope.dadosJogo.perguntaAtual].respCorreta - 1;
          }
        };

      });
})();