(function() {
    'use strict';

	angular.module('jogo')

	.controller('FimJogoCtrl', function($scope, $ionicLoading, $window, QuestaoAPI, UsuarioAPI, JogoAPI, FaseAPI) {
		if (typeof $window.analytics !== 'undefined') { $window.analytics.trackView('Fim Jogo');}

		if (window.localStorage['respostas']){
		QuestaoAPI.salvaHistoricoFase().success(function(data){
		  window.localStorage['respostas'] = JSON.stringify([]);
		})
		}

		FaseAPI.salvaFase($scope.dadosFases).success(function(){
		  window.localStorage['atualizaFase'] = false;
		  $ionicLoading.hide();
		}).error(function(){
		  $ionicLoading.hide();
		});

		JogoAPI.finalizaJogo(angular.fromJson(window.localStorage['token'])).success(function(data){
		$scope.conquista = data;
		$ionicLoading.hide();
		}).error(function(){
		$ionicLoading.hide();
		})
	});
})();
