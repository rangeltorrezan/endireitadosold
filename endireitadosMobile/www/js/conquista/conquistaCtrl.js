(function() {
    'use strict';

	angular.module('conquista')

	.controller('ConquistaCtrl', function($scope, $ionicLoading, $window, ConquistaAPI) {
	  if (typeof $window.analytics !== 'undefined') { $window.analytics.trackView('Lista Conquista');}

	  ConquistaAPI.getConquistas(angular.fromJson(window.localStorage['token'])).success(function(data){
	    $scope.conquista = data;
	  }).error(function(){
	  
	  })
	});
})();